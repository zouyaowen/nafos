package nafos.core.mode.shutdown;

/**
* @author 作者 huangxinyu 
* @version 创建时间：2018年4月9日 下午3:45:14 
* http请求过来的前置处理器
*/
public interface ShutDownHandleInterface {
	
	void run();

}
